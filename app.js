var http = require('http');
var dt = require('./first1.js');
var events = require('events');
var fs = require('fs');
var formidable = require('formidable');
var eventEmitter = new events.EventEmitter();

http.createServer(function (req, res) {
    if (req.url=="/fileupload") {
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            var oldPath = files.fileupload.path;
            var newPath = "C:/uploadfiles/" + files.fileupload.name;

            fs.renameSync(oldPath, newPath, function(err) {
                if (err) throw err;
                res.write('File uploaded and moved!');
                res.end();                
            });
        })
    } else {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.write("<form action='fileupload' method='post' enctype='multipart/form-data'>");
        res.write('<input type="file" name="fileupload"/><br/>');
        res.write('<button type="submit">Upload</button>');
        res.write('</form>');
        return res.end();
    }

}).listen(3000, function () {
    console.log('Server is running on port 3000!');
});
